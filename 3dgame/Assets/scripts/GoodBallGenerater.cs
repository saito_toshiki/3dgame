using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoodBallGenerater : MonoBehaviour
{
    public GameObject Goodball;
    float span = 1.0f;
    float delta = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.delta += Time.deltaTime;
        if (this.delta > this.span)  //矢の数が2個以上の場合1個消す
        {
           


            this.delta = 0;
            GameObject go = Instantiate(Goodball) as GameObject;
            int pz = Random.Range(-4, 5);//矢の出てくるZ座標の範囲を「-6〜6」の間をランダム
            int py = Random.Range(1, 3);
            go.transform.position = new Vector3(20, py, pz);
        }
    }
}
