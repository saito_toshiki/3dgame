using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameDirector : MonoBehaviour
{
    public GameObject score;
    int count = 0;

    GameObject hpGauge;

   int hp = 10;

    // Start is called before the first frame update
    void Start()
    {
       

        this.hpGauge = GameObject.Find("hpGauge");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ScoreUp()
    {
        count++;

        this.score.GetComponent<Text>().text = "Score:" + count;

        if (count == 20)
        {
            SceneManager.LoadScene("GameClearScene");
        }
    }

    public void DecreaseHp()
    {
       

        

        this.hpGauge.GetComponent<Image>().fillAmount -= 0.1f;

        hp -= 1;

       if(hp == 0)
       {
            SceneManager.LoadScene("GameOverScene");
       }
    }
}
