using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class badController : MonoBehaviour
{
    GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        this.player = GameObject.Find("player");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(-0.05f, 0, 0);

        if (transform.position.x < -25.0f)
        {
            Destroy(gameObject);
        }

        Vector3 p1 = transform.position;　　//矢の中心座標
        Vector3 p2 = this.player.transform.position;　　　　//プレイヤの中心座標
        Vector3 dir = p1 - p2;
        float d = dir.magnitude;
        float r1 = 0.5f;　//矢の半径
        float r2 = 0.5f; //プレイヤの半径

        if (d < r1 + r2)
        {
            GameObject director = GameObject.Find("GameDirector");
            director.GetComponent<GameDirector>().DecreaseHp();

            Destroy(gameObject);
        }
    }
}
